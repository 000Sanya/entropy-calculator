use yew::{Component, ComponentLink, Renderable, Html, App, html};
use stdweb::web::{IElement, INode, IParentNode, document, INonElementParentNode};

fn main() {
    let app = App::<MainModel>::new();
    let body = document().query_selector("body").unwrap().unwrap();
    let mount_point = document().create_element("div").unwrap();
    mount_point.class_list()
        .add("uk-container").unwrap();
    body.append_child(&mount_point);
    app.mount(mount_point);
    yew::run_loop();
}

struct MainModel {
    values: Vec<Option<f64>>,
    enthropy: f64,
    sum: f64,
    is_formulas: bool,
}

impl MainModel {
    fn calculate_enthropy(&mut self) {
        self.enthropy = -self.values
            .iter()
            .filter_map(|x| *x)
            .map(|x| x * x.log2())
            .sum::<f64>();
    }

    fn calculate_sum(&mut self) {
        self.sum = self.values
            .iter()
            .filter_map(|x| *x)
            .sum::<f64>();
    }

    fn render_field(&self, i: usize, value: &Option<f64>) -> Html<Self> {
        let value = value.clone().unwrap_or(0.0);
        html! {
            <div class="uk-margin",>
                <div class="uk-inline",>
                    <a
                        class="uk-form-icon uk-form-icon-flip",
                        data-uk-icon="icon: minus-circle",
                        onclick=|e| Msg::Delete(i),
                    ></a>
                    <input
                        type="text",
                        value=value,
                        oninput=|e| Msg::Edit(i, e.value),
                        class="uk-input",
                    />
                </div>
            </div>
        }
    }

    fn render_image(&self) -> Html<Self> {
        if self.is_formulas {
            html!{
                <img src="img/formulas.png",></img>
            }
        } else {
            html!{
                <p> </p>
            }
        }
    }
}

enum Msg {
    New,
    Edit(usize, String),
    Delete(usize),
}

impl Component for MainModel {
    type Message = Msg;
    type Properties = ();

    fn create(props: Self::Properties, link: ComponentLink<Self>) -> Self {
        Self {
            values: vec![],
            enthropy: 0.0,
            sum: 0.0,
            is_formulas: false,
        }
    }

    fn update(&mut self, msg: Self::Message) -> bool {
        let res = match msg {
            Msg::New => {
                self.values.push(Some(0.0));
                true
            },
            Msg::Edit(i, value) => {
                self.is_formulas = value == "form";
                self.values[i] = value
                    .parse()
                    .ok();
                true
            },
            Msg::Delete(i) => {
                self.is_formulas = false;
                self.values.remove(i);
                true
            }
        };
        self.calculate_enthropy();
        self.calculate_sum();
        res
    }
}

impl Renderable<Self> for MainModel {
    fn view(&self) -> Html<Self> {
        html! {
            <div>
                <p>{format!("Энтропия: {}", self.enthropy)}</p> <br />
                { self.render_image() }
                <p>{format!("Сумма вероятностей: {}", self.sum)}</p> <br />
                <form>
                    {
                        for self.values.iter()
                            .enumerate()
                            .map(|(i, value)| {
                                self.render_field(i, value)
                            })
                    }

                </form>
                <button onclick=|_| Msg::New, class="uk-button uk-button-default",>
                    { "Добавить поле" }
                </button>
            </div>
        }
    }
}
